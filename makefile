HIER = --hierarchical

default: r

comp:
	rm -rf obj_dir
	verilator --cc --debug --dump-treei 9 ip.sv test.sv ${HIER} --top-module test

obj_dir/Vtest__ALL.a: comp
	make -C obj_dir -f Vtest_hier.mk
	cp $@ obj_dir/libVtest__ALL.a

sim_main.o:
	g++  -I.  -MMD -fPIC -I/home/harriszh/.local/share/verilator/include -I/home/harriszh/.local/share/verilator/include/vltstd -DVM_COVERAGE=1 -DVM_SC=0 -DVM_TRACE=1 -DVM_TRACE_FST=0 -Wno-sign-compare -Wno-uninitialized -Wno-unused-but-set-variable -Wno-unused-parameter -Wno-unused-variable -Wno-shadow -I./obj_dir     -std=gnu++11 -MMD -MP -DVL_DEBUG=1 -Os -fstrict-aliasing -c -o sim_main.o ./sim_main.cpp obj_dir/Vtest__ALL.a

verilated.o:
	g++  -I.  -MMD -fPIC -I/home/harriszh/.local/share/verilator/include -I/home/harriszh/.local/share/verilator/include/vltstd -DVM_COVERAGE=1 -DVM_SC=0 -DVM_TRACE=1 -DVM_TRACE_FST=0 -Wno-sign-compare -Wno-uninitialized -Wno-unused-but-set-variable -Wno-unused-parameter -Wno-unused-variable -Wno-shadow      -std=gnu++11 -MMD -MP -DVL_DEBUG=1 -Os -c -o verilated.o /home/harriszh/.local/share/verilator/include/verilated.cpp

verilated_cov.o:
	g++  -I.  -MMD -fPIC -I/home/harriszh/.local/share/verilator/include -I/home/harriszh/.local/share/verilator/include/vltstd -DVM_COVERAGE=1 -DVM_SC=0 -DVM_TRACE=1 -DVM_TRACE_FST=0 -Wno-sign-compare -Wno-uninitialized -Wno-unused-but-set-variable -Wno-unused-parameter -Wno-unused-variable -Wno-shadow      -std=gnu++11 -MMD -MP -DVL_DEBUG=1 -Os -c -o verilated_cov.o /home/harriszh/.local/share/verilator/include/verilated_cov.cpp

verilated_vcd_c.o:
	g++  -I.  -MMD -fPIC -I/home/harriszh/.local/share/verilator/include -I/home/harriszh/.local/share/verilator/include/vltstd -DVM_COVERAGE=1 -DVM_SC=0 -DVM_TRACE=1 -DVM_TRACE_FST=0 -Wno-sign-compare -Wno-uninitialized -Wno-unused-but-set-variable -Wno-unused-parameter -Wno-unused-variable -Wno-shadow      -std=gnu++11 -MMD -MP -DVL_DEBUG=1 -Os -c -o verilated_vcd_c.o /home/harriszh/.local/share/verilator/include/verilated_vcd_c.cpp

#obj_dir/Vtest:   obj_dir/Vtest__ALL.a sim_main.o verilated.o verilated_cov.o verilated_vcd_c.o
	#g++  obj_dir/Vtest__ALL.a verilated.o verilated_cov.o verilated_vcd_c.o sim_main.o -o $@               # doesn't work
	#g++ verilated.o verilated_cov.o verilated_vcd_c.o sim_main.o ./obj_dir/Vip/Vip__ALL.o ./obj_dir/Vip/ip.o ./obj_dir/Vtest__ALL.o -o $@            #work
	#g++ verilated.o verilated_cov.o verilated_vcd_c.o sim_main.o -lVtest__ALL -L./obj_dir -o $@


obj obj_dir/Vtest : obj_dir/Vtest__ALL.a ./sim_main.cpp
	g++  -I.  -MMD -I/home/harriszh/.local/share/verilator/include -I/home/harriszh/.local/share/verilator/include/vltstd -DVM_COVERAGE=1 -DVM_SC=0 -DVM_TRACE=1 -DVM_TRACE_FST=0 -Wno-sign-compare -Wno-uninitialized -Wno-unused-but-set-variable -Wno-unused-parameter -Wno-unused-variable -Wno-shadow -I./obj_dir     -std=gnu++11 -MMD -MP -DVL_DEBUG=1 -Os -fstrict-aliasing  -o obj_dir/Vtest ./sim_main.cpp /home/harriszh/.local/share/verilator/include/verilated.cpp /home/harriszh/.local/share/verilator/include/verilated_cov.cpp /home/harriszh/.local/share/verilator/include/verilated_vcd_c.cpp obj_dir/Vtest__ALL.a



r: obj_dir/Vtest
	obj_dir/Vtest


comp1:
	rm -rf obj_dir
	verilator  --exe --cc --debug --dump-treei 9 ip.sv test.sv ${HIER} --top-module test sim_main.cpp

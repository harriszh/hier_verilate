# this example is used to investigate verilator

## clock

There is one top clock clk as the primary input

## rstn

There is one reset rstn as the primary reset

## output

sum = u0.d + u1.d + u2.d + d3.d


## partition

expecting 5 partions:  test,  u0, u1, u2, u3

module ip #(DWIDTH=32) (input clk, input rstn, input [DWIDTH-1:0] cnt, input [7:0] step, output reg [DWIDTH-1:0] d, output clko);

/*verilator hier_block*/

assign clko = clk;

always @(posedge clk or negedge rstn) begin
	if(!rstn) begin
		d <= 0;
	end else begin
		d <= cnt + {{24{1'b0}}, step};
	end
end


endmodule

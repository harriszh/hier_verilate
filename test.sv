module test(input clk, input rstn, output [63:0] sum, output [31:0] d0, output reg [31:0] d00);

parameter DWIDTH=32;

reg [DWIDTH-1:0] cnt;
//wire [DWIDTH*2-1:0] sum;
//wire [DWIDTH-1:0] d0;
wire [DWIDTH-1:0] d1;
wire [DWIDTH-1:0] d2;
wire [DWIDTH-1:0] d3;

//reg [DWIDTH-1:0] d00;

wire [3:0] clko;

always @(posedge clk or negedge rstn) begin
	if(!rstn) begin
		cnt <= 0;
	end else begin
		cnt <= cnt + 1;
	end
end

always @(posedge clk) begin
	if(cnt == 32'hffff) begin
		$finish;
	end
end

assign sum = {{32{1'b0}}, d0} + {{32{1'b0}}, d1} + {{32{1'b0}}, d2} + {{32{1'b0}}, d3};

always @(posedge clko[0]) begin
	d00 <= d0;
end


//ip #(.DWIDTH(DWIDTH)) u0(clk, rstn, cnt, 1, d0);
//ip #(.DWIDTH(DWIDTH)) u1(clk, rstn, cnt, 2, d1);
//ip #(.DWIDTH(DWIDTH)) u2(clk, rstn, cnt, 3, d2);
//ip #(.DWIDTH(DWIDTH)) u3(clk, rstn, cnt, 4, d3);

ip u0(clk, rstn, cnt, 1, d0, clko[0]);
ip u1(clk, rstn, cnt, 2, d1, clko[1]);
ip u2(clk, rstn, cnt, 3, d2, clko[2]);
ip u3(clk, rstn, cnt, 4, d3, clko[3]);

endmodule
